#! /bin/bash
# @a221603er ASIX-M01
# Febrer 2023
#
# exemple de CASE
#---------------------------------------------------------------------

case $1 in
	"dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
		echo "es laborable"
		;;
	"disabte"|"diumenge")
		echo "es festiu"
		;;
	*)
		echo "$1 eso no es un dia chavaaal"
		;;
esac

exit 0


case $1 in
	[aeiuo])
		echo "$1 es una vocal"
		;;
	[qwrtypsdfghjklzxcvbnm])
		echo "$1 es una consonante"
		;;
	*)
		echo " es una altra cosa (no binario)"
		;;

esac

exit 0


case $1 in
	"pere"|"pau"|"joan")
		echo "es un nen"
		;;
	"marta"|"anna"|"julia")
		echo "es una nena"
		;;
	*)
		echo "no binari"
		;;
esac
exit 0


