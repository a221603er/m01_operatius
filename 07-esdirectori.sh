#! /bin/bash
# @a221603er ASIX-M01
# Febrer 2023
#
# exemple de VALIDAR NOTA 
# $ prog nota
#---------------------------------------------------------------------

#1) validar que existe un argumnet

if [ $# -ne 1 ]
then
	echo "Error: numero args incorrecte"
	echo "Usage: $0 dir"
	exit 1
fi

#2) validar rang nota
if  [ ! -d $1 ]
then
	echo "error: $1 no es un directorio"
	echo "usage: $0 dir"
	exit 2
fi

#xixa
dir=$1
ls $dir
exit 0

