#! /bin/bash
# @a221603er ASIX-M01
# marzo 2023
#
# llistar-dir.sh dir
# fa un 'ls' del directori rebut
# verificar 1 arg , i que és un dir
#------------------------------------------------------------------
#1)
ERR_NARGS=1
ERR_NODIR=2
#1) validar arguments
if [ $# -ne 1 ]
then
	echo "ERROR: numero args no valid"
	echo "usage: $0 dir"
	exir $ERR_NARGS
fi
dir=$1
#2
if [ -d $dir ]
then
	echo "Error: $dir no es un directori"
	echo "usage:
