#! /bin/bash
# @a221603er ASIX-M01
# marzo 2023
#
# exemple de bucle while
#------------------------------------------------------------------


#7) numera i mostra en majuscules l'entrada estandard
num=1
while read -r line
do
	echo "$num: $line" | tr 'a-z' 'A-Z'
	((num++))
done 
exit 0



#6) mostar stdin linea a linea fins
#token
read -r line
while [ "$line" != "FI" ]
do
	echo "$line"
	read -r line
done 
exit 0



# 5) numerar stadin linea a linea
num=1
while read -r linea
do
	echo"$num: $linea"
	((num++))
done
exit 0 



# 4) processar entrada estandard linea
#    a linea
while read -r line
do
	echo $line
done

exit 0


#3) ietarr per la llista d'arguments
while [	-n "$1" ]
do
	echo "$1 $# $*"
	shift
done
exit 0 


#2) comptador decreixent n(arg)...0
MIN=0
num=$1

while [ $num -ge $MIN ]
do
	echo "$num"
	((num--))
done
exit 0


#1) mostar un comptador del 1 al 10
MAX=10
num=1
while [ $num -le $MAX ]
do
	echo "$num"
	((num++))
done
exit 0 
