#! /bin/bash
# @a221603er ASIX-M01
# Febrer 2023
#
# exemple if: indicar si es major de edat o menor 
# $ prog edat
#---------------------------------------------------------------------

#1) validar que existe un argumnet

if [ $# -ne 1 ]
then
	echo "Error: numero args incorrecte"
	echo "Usage: $0 edat"
	exit 1
fi

#2) xixa
edat=$1
if [ $edat -lt 18 ]
then
  echo "edat $edat es menor d'edat"
elif [ $edat -lt 65 ]	
then
	echo "edat $edat es poblacio activa"
else

	echo "edat $edat es jubilado"
fi
exit 0





