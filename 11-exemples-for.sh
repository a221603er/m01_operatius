#! /bin/bash
# @a221603er ASIX-M01
# marzo 2023
#
# exemple de for
#------------------------------------------------------------------

#8) llistar tots el logins numerats
llista_login="pere marta anna"
num=1
for login in $llista_logins
do
	echo "$num: $login"
	((num++))
done
exit 0



#7) llista el fichers del directori actiu numerats 
llista_noms=$(ls)
num=1
for nom in $llista
do
	echo "$num: $nom"
	((num++))
done
exit 0






#6) numerar els arguments
num=1
for arg in $*
do
	echo "$num: $arg"
done
exit 0



#5) iterar per a un conjunt d'elements
for arg in "$*"
do
	echo "$arg"
done
exit 0



#3) iterar pel valor d'una variable
llistat=$(ls)
for nom in $llistat
do
	echo "$nom"
done
exit 0

#2) iterar per un conjunt d'elemts
for nom in "pere marta pau anna"
do
	echo "$nom"
done
exit 0

#1) iterar per a un conjunt d'elements
for nom in "pere" "marta" "pau" "anna"
do
	echo "$nom"
done
exit 0


