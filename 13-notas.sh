#! /bin/bash
# @a221603er ASIX-M01
# marzo 2023
#
# exemple de for
#------------------------------------------------------------------
#codi

for nota in $*
do
	if ! [ $nota -ge 0 -a $nota -le 10 ];then
		echo "error: $nota no entre [1-10]" >> /dev/stderr
	elif [ $nota -lt 5 ];then
		echo "$nota has tret un $nota, suspendido"
	elif [ $nota -lt 7 ];then
		echo "$nota has tret un $nota, aprobado"
	elif [ $nota -lt 9 ];then
		echo "$nota has tret un $nota, notable"
	else
		echo "$nota has tret un $nota, excelente"
	fi
	
done
exit 0
