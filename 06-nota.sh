#! /bin/bash
# @a221603er ASIX-M01
# Febrer 2023
#
# exemple de VALIDAR NOTA 
# $ prog nota
#---------------------------------------------------------------------
#1) validar que existe un argumnet

if [ $# -ne 2 ]
then
	echo "Error: numero args incorrecte"
	echo "Usage: $0 nota"
	exit 1
fi

#2) validar rang nota
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
	echo "error nota $1 no valida"
	echo "nota pren valors de 0 a 10"
	echo "usage: $0 nota"
	exit 2
fi



#xixa
nota=$1
if [ $nota -lt 5 ]; then
	echo "la nota
