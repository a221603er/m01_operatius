#! /bin/bash
# @a221603er ASIX-M01
# marzo 2023
#
# llistar-dir.sh dir
# fa un 'ls' del directori rebut
# verificar 1 arg , i que és un dir
#------------------------------------------------------------------
#1)
ERR_NARGS=1
ERR_NODIR=2
#1) validar arguments
if [ $# -ne 1 ]
then
	echo "ERROR: numero args no valid"
	echo "usage: $0 dir"
	exit $ERR_NARGS
fi
dir=$1
#2
if [ -d $dir ]
then
	echo "Error: $dir no es un directori"
	echo "usage: $0 dir"
	exit $ERR_NODIR
fi
#3 xixa
dir=$1
comptador=1
llista=$(ls $dir)
for elem in $llista
do
	echo $comptador: $elem
	((comptador++))
done

exit 0
