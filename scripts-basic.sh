#!/bin/bash


for dia in "$@"
do
  case "$dia" in
    dilluns|dimarts|dimecres|dijous|divendres)
      laborables=$((laborables+1))
      ;;
    dissabte|diumenge)
      festius=$((festius+1))
      ;;
    *)
      echo "Error: $dia no és un dia de la setmana vàlid" >&2
      ;;
  esac
done

echo "Hi ha $laborables dies laborables i $festius dies festius."



exit 0

linia=1
while read -r entrada; do
	if [ -z "$entrada" ]; then  # si s'ha arribat al final de l'entrada, sortir del bucle
    	break
	fi
	echo "$linia $entrada"  # mostrar el número de línia i la línia llegida
	linia=$((linia+1))
done

